<?php namespace Cuatrokb\AdminAuth;

use Cuatrokb\AdminAuth\Activation\Providers\ActivationServiceProvider;
use Cuatrokb\AdminAuth\Console\Commands\AdminAuthInstall;
use Cuatrokb\AdminAuth\Http\Middleware\ApplyUserLocale;
use Cuatrokb\AdminAuth\Http\Middleware\CanAdmin;
use Cuatrokb\AdminAuth\Http\Middleware\RedirectIfAuthenticated;
use Cuatrokb\AdminAuth\Providers\EventServiceProvider;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AdminAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            AdminAuthInstall::class,
        ]);

        $this->loadTranslationsFrom(resource_path('lang/vendor/cuatrokb'), 'cuatrokb');
        // $this->loadViewsFrom(__DIR__ . '/../resources/views', 'cuatrokb/admin-auth');

        $this->app->register(ActivationServiceProvider::class);
        $this->app->register(EventServiceProvider::class);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../install-stubs/config/admin-auth.php' => config_path('admin-auth.php'),
            ], 'config');

            if (!glob(base_path('database/migrations/*_create_admin_activations_table.php'))) {
                $this->publishes([
                    __DIR__ . '/../install-stubs/database/migrations/create_admin_activations_table.php' => database_path('migrations') . '/2017_08_24_000000_create_admin_activations_table.php',
                ], 'migrations');
            }

            if (!glob(base_path('database/migrations/*_create_admin_password_resets_table.php'))) {
                $this->publishes([
                    __DIR__ . '/../install-stubs/database/migrations/create_admin_password_resets_table.php' => database_path('migrations') . '/2017_08_24_000000_create_admin_password_resets_table.php',
                ], 'migrations');
            }

            if (!glob(base_path('database/migrations/*_create_admin_users_table.php'))) {
                $this->publishes([
                    __DIR__ . '/../install-stubs/database/migrations/create_admin_users_table.php' => database_path('migrations') . '/2017_08_24_000000_create_admin_users_table.php',
                ], 'migrations');
            }

            $this->publishes([
                __DIR__ . '/../resources/lang' => resource_path('lang/vendor/cuatrokb'),
            ], 'lang');

            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views')
            ], 'views');
        }

        $this->app->bind(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            \Cuatrokb\AdminAuth\Exceptions\Handler::class
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../install-stubs/config/admin-auth.php', 'admin-auth'
        );

        if (config('admin-auth.use_routes', true)) {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        }

        if (config('admin-auth.use_routes', true) && config('admin-auth.activations.self_activation_form_enabled',
                true)) {
            $this->loadRoutesFrom(__DIR__ . '/../routes/activation-form.php');
        }

        //This is just because laravel does not provide it by default, however expect in AuthenticationException that it exists
        if(!Route::has('login')) {
            Route::get('/login', function () {
                return Redirect::route('cuatrokb/admin-auth::admin/login');
            })->name('login');
        }
        //This is just because in welcome.blade.php someone was lazy to check if also register route exists and ask only for login
        if(!Route::has('register')) {
            Route::get('/register', function () {
                return Redirect::route('cuatrokb/admin-auth::admin/login');
            })->name('register');
        }

        app(\Illuminate\Routing\Router::class)->pushMiddlewareToGroup('admin', CanAdmin::class);
        app(\Illuminate\Routing\Router::class)->pushMiddlewareToGroup('admin', ApplyUserLocale::class);
        app(\Illuminate\Routing\Router::class)->aliasMiddleware('guest.admin', RedirectIfAuthenticated::class);
    }
}
